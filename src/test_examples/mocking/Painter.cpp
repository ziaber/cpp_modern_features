//
// Created by Marcin Ziaber on 30/10/2019.
//

#include <memory>
#include "../../../include/test_examples/mocking/Painter.h"

int Painter::getTurtleXPosition() {
    return this->turtle->GetX();
}

Painter::Painter(const std::shared_ptr<Turtle> &turtle) : turtle(turtle) {}
