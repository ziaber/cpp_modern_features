
#include <iostream>
#include "../include/SmartPointers.h"
#include "../include/templates/VariadicTemplate.h"
#include "../include/templates/TemplateAliases.h"
#include "../include/templates/CompileTimeFactorial.h"
#include "../include/TypeInference.h"

using namespace std;


int main(int argc, char *argv[]) {
    SmartPointers smartPointers;
//    smartPointers.uniquePtrExample();

//    VariadicTemplate variadicTemplate;
//    variadicTemplate.variadicTemplateExample();
//    TemplateAliases templateAliases;
//    templateAliases.aliasesExample();

    cout << factorial<4>::value << endl;

    TypeInference typeInference;
    typeInference.decltypeExample();
    return 0;
}