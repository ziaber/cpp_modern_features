//
// Created by Marcin Ziaber on 2019-07-09.
//

#include "../include/Containers.h"
#include <vector>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <list>
#include <string>
#include <algorithm>

using namespace std;

void Containers::containersExample() {
    vector<int> v = {1, 2, 3, 4};
    v.push_back(5);
    int fromVector = v[0];

    list<double> l = {1.0, 2.0, 3.0};
    l.push_back(5.0);
    double fromList = *l.begin();

    unordered_map<string, int> phone_book = {
            {"David Hume",                      123456},
            {"Karl Popper",                     234567},
            {"Bertrand Arthur William Russell", 345678}
    };
    int phoneNumber = phone_book["David Hume"];


    unordered_set<int> mySetOfInts = {10, 20, 30};
    mySetOfInts.find(10);

    sort(v.begin(), v.end());
    reverse(v.begin(), v.end());//etc
}
