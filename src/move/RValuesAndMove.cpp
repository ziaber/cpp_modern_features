//
// Created by Marcin Ziaber on 30/10/2019.
//

#include "../../include/move/RValuesAndMove.h"
#include <iostream>

using namespace std;

vector<int> RValuesAndMove::doubleValues(const vector<int> &v) {
    vector<int> new_values;
    new_values.reserve(v.size());
    for (auto &element : v) {
        new_values.push_back(2 * element);
    }
    return new_values;
}

void RValuesAndMove::moveProblem() {
    vector<int> v;
    for (int i = 0; i < 100; i++) {
        v.push_back(i);
    }
    //where is the performance issue here?
    v = doubleValues(v);
}

void RValuesAndMove::printReference(const string &str) {
    cout << "printed by lvalue reference" << endl;
}

void RValuesAndMove::printReference(string &&str) {
    cout << "printed by rvalue reference" << endl;
}

void RValuesAndMove::rvslvalue() {
    int a;
    a = 1; // here, a is an lvalue

    int x;
    getRef(x) = 4; //getRef(x) is an lvalue, we can assign to it an rvalue 4

    getValue(a);//rvalue

    string me("alex");//constructor

    // calls the first printReference function, taking an lvalue reference
    printReference(me);

    // calls the second printReference function, taking a mutable rvalue reference
    printReference(getName());
}

int &RValuesAndMove::getRef(int &x) {
    return x;
}

int RValuesAndMove::getValue(int x) {
    return x;
}

string RValuesAndMove::getName() {
    return "Marcin";
}

void RValuesAndMove::moveOperatorExample() {
    auto uniqPtr = std::make_unique<int>(1998);
//    not compiling
//    somePointerOperation(uniqPtr);

//uniqPtr is a wrapper over ptr, ptr will be passed to new uniqPtr wrapper
    somePointerOperation(move(uniqPtr));
}

void RValuesAndMove::somePointerOperation(unique_ptr<int> pointer) {
    //some operation
}

