//
// Created by Marcin Ziaber on 24/08/2019.
//

#include "../../include/templates/ClassTemplate.h"

template<class T>
void ClassTemplate<T>::classTemplateExample() {
    auto maxDouble = this->max(1.0, 2.0);
    auto maxString = this->max("1.0", "2.0");
}