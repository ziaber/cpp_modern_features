//
// Created by Marcin Ziaber on 24/08/2019.
//

#include "../../include/templates/VariadicTemplate.h"

void VariadicTemplate::variadicTemplateExample() {
    this->printAll("ala", 1.0, 2L);
}
