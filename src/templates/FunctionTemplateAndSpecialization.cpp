//
// Created by Marcin Ziaber on 24/08/2019.
//

#include "../../include/templates/FunctionTemplateAndSpecialization.h"

void FunctionTemplateAndSpecialization::functionTemplateExample() {

    auto maxDouble = this->max(1.0, 2.0);
    auto maxString = this->max("1.0", "2.0");

    auto maxBool = this->max(true, false);
}
