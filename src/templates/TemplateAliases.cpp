//
// Created by Marcin Ziaber on 24/08/2019.
//

#include <string>
#include "../../include/templates/TemplateAliases.h"

void TemplateAliases::aliasesExample() {
    StrMap<long> strMap;
    strMap.insert({10L, "ala"});

    StrMap<double> doubleStrMap;
    doubleStrMap.insert({1.0, "ala"});
}
