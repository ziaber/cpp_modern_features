//
// Created by Marcin Ziaber on 2019-07-09.
//

#include "../include/SmartPointers.h"
#include <memory>

using namespace std;

void SmartPointers::uniquePtrExample() {
    unique_ptr<int> p1(new int(5));
//    unique_ptr<int> p2 = p1;  // Compile error.
    unique_ptr<int> p3 = std::move(p1);  // Transfers ownership. p3 now owns the memory and p1 i

    assert(p1 == nullptr); //true

    uniquePtrAsArgumentExample(p3); //siply pass by reference

    assert(p3 == nullptr); //false

    uniquePtrAsArgumentMoveExample(move(p3));

    assert(p3 == nullptr); //true
}

void SmartPointers::uniquePtrAsArgumentExample(const unique_ptr<int> &arg) {
    //do stuff
}

void SmartPointers::uniquePtrAsArgumentMoveExample(unique_ptr<int> arg) {
    //take ownership and do stuff

    //delete the resource
}

void SmartPointers::sharedPtrExample() {
    shared_ptr<int> p1(new int(5));

    sharedPointerAsValueExample(p1);
}

void SmartPointers::sharedPointerAsValueExample(const shared_ptr<int> arg) {
    //do some stuff
}
