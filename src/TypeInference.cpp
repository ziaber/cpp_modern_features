//
// Created by Marcin Ziaber on 2019-07-09.
//

#include "../include/TypeInference.h"
#include <initializer_list>
#include <vector>
#include <iostream>

using namespace std;

void TypeInference::typeInferenceExample() {
    auto a = 3.14; // double
    auto b = 1; // int
    auto &c = b; // int& - reference to int
    auto d = {0}; // std::initializer_list<int>
    auto &&e = 1; // int&&
    auto &&f = b; // int&
    auto g = new auto(123); // int*
    const auto h = 1; // const int
    auto i = 1, j = 2, k = 3; // int, int, int

    //Wrong examples
//    auto l = 1, m = true, n = 1.61; // error -- `l` deduced to be int, `m` is bool
//    auto o; // error -- `o` requires initializer

    //Especially useful with iterators
    //Standard
    std::vector<int> v;
    std::vector<int>::const_iterator cit = v.cbegin();

    //vs
    auto citWithAuto = v.cbegin();

    //method type inference
    auto result = add(1, 2);
}

void TypeInference::autoWithRangeLoopExample() {

    std::vector<int> v;
    for (auto &element : v) {
        //stuff to do
    }

}

void TypeInference::decltypeExample() {
    int a = 1; // `a` is declared as type `int`
    decltype(a) b = a; // `decltype(a)` is `int`
    const int &c = a; // `c` is declared as type `const int&`
    decltype(c) d = a; // `decltype(c)` is `const int&`
    decltype(123) e = 123; // `decltype(123)` is `int`
    int &&f = 1; // `f` is declared as type `int&&`
    decltype(f) g = 1; // `decltype(f) is `int&&`
    decltype((a)) h = g; // `decltype((a))` is int&
}
