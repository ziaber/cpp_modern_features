//
// Created by Marcin Ziaber on 30/10/2019.
//

#include "../include/Lambdas.h"
#include "../include/User.h"
#include <list>

using namespace std;

void Lambdas::lambdasExample() {
    auto x = 10;
    //capture by value
    auto addToX = [=](int y) { return x + y; };
    int z = addToX(2); //=12;

    auto addToXCaptureReference = [&](int y) { return x + y; };
    addToXCaptureReference(2); //=12;

    //only z visable
    auto addToZ = [&z](int y) { return z + y; };
    addToZ(2); //=14;

    //nothing visible

    auto addTwoNumbers = [](int x, int y) { return x + y; };

    addTwoNumbers(1, 2); //=3;

    //generic lambdas
    auto identity = [](auto x) { return x; };
    int three = identity(3); // == 3 of type int
    double four = identity(4.0);

    //helpful in containers, comparators, sort(), copy_if() etc.
    list<User> users = {new User(2), new User(1)};
    //lowestIdUser = User(1)
    auto lowestIdUser = min_element(users.begin(), users.end(),
                                    [](const User &u1, const User &u2) {
                                        return u1.getId() < u2.getId();
                                    });
}
