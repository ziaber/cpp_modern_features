//
// Created by Marcin Ziaber on 30/10/2019.
//

#include "../include/User.h"

User::User(int id) : id(id) {}

int User::getId() const {
    return id;
}
