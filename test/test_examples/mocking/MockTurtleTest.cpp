//
// Created by Marcin Ziaber on 30/10/2019.
//

#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include "MockTurtle.h"
#include "../../../include/test_examples/mocking/Painter.h"

using ::testing::AtLeast;
using ::testing::Return;
using namespace std;

TEST(PainterTest, CanDrawSomething) {
    //given
    auto mockedTurtle = make_shared<MockTurtle>();
    EXPECT_CALL(*mockedTurtle, GetX())
            .Times(AtLeast(1))
            .WillOnce(Return(10));
    Painter painter(mockedTurtle);

    //when
    auto result = painter.getTurtleXPosition();

    //then
    ASSERT_EQ(result, 10);
}