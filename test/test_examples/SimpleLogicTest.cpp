//
// Created by Marcin Ziaber on 2019-05-11.
//

#include <gtest/gtest.h>
#include "../../include/test_examples/SimpleLogicForTest.h"

//cmake .
//make
//ctest .

TEST(SimpleLogicTest, shouldAddTwoNumbers) {
//given
    auto a = 1;
    auto b = 100;

//when
    auto value = SimpleLogicForTest::addTwoNumbers(a, b);

//then
    ASSERT_EQ(value, 101);
}