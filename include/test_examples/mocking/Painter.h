//
// Created by Marcin Ziaber on 30/10/2019.
//

#ifndef CPP_MODERN_FEATURES_PAINTER_H
#define CPP_MODERN_FEATURES_PAINTER_H


#include "Turtle.h"

class Painter {
private:
    std::shared_ptr<Turtle> turtle;

public:
    Painter(const std::shared_ptr<Turtle> &turtle);

    int getTurtleXPosition();

};


#endif //CPP_MODERN_FEATURES_PAINTER_H
