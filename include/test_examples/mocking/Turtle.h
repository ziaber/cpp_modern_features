//
// Created by Marcin Ziaber on 30/10/2019.
//

#ifndef CPP_MODERN_FEATURES_TURTLE_H
#define CPP_MODERN_FEATURES_TURTLE_H


class Turtle {//interface
public:
    virtual ~Turtle() = default;;
    virtual void PenUp() = 0;
    virtual void PenDown() = 0;
    virtual void Forward(int distance) = 0;
    virtual void Turn(int degrees) = 0;
    virtual void GoTo(int x, int y) = 0;
    virtual int GetX() const = 0;
    virtual int GetY() const = 0;
};


#endif //CPP_MODERN_FEATURES_TURTLE_H
