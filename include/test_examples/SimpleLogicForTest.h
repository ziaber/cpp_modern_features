//
// Created by Marcin Ziaber on 29/10/2019.
//

#ifndef CPP_MODERN_FEATURES_SIMPLELOGICFORTEST_H
#define CPP_MODERN_FEATURES_SIMPLELOGICFORTEST_H


class SimpleLogicForTest {
public:
    static int addTwoNumbers(int a, int b);

};


#endif //CPP_MODERN_FEATURES_SIMPLELOGICFORTEST_H
