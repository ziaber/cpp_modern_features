//
// Created by Marcin Ziaber on 30/10/2019.
//

#ifndef CPP_MODERN_FEATURES_RVALUESANDMOVE_H
#define CPP_MODERN_FEATURES_RVALUESANDMOVE_H

#import "../../../../../../Library/Developer/CommandLineTools/usr/include/c++/v1/vector"
#import <string>
#include <iostream>

using namespace std;

class RValuesAndMove {
private:
    vector<int> doubleValues(const vector<int> &v);

    void printReference(const string &str);

    void printReference(string &&str);

    int &getRef(int &x);

    int getValue(int x);

    string getName();

    void somePointerOperation(unique_ptr<int> pointer);

public:
    void moveProblem();

    void rvslvalue();

    void moveOperatorExample();
};


#endif //CPP_MODERN_FEATURES_RVALUESANDMOVE_H
