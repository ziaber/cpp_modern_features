//
// Created by Marcin Ziaber on 30/10/2019.
//

#ifndef CPP_MODERN_FEATURES_ARRAYWRAPPER_H
#define CPP_MODERN_FEATURES_ARRAYWRAPPER_H


class ArrayWrapper {
private:
    int *values;
    int size;

public:
    ArrayWrapper() : values(new int[64]), size(64) {}

    ArrayWrapper(int size) : values(new int[size]), size(size) {}

    // copy constructor
    ArrayWrapper(const ArrayWrapper &other) : values(new int[other.size]), size(other.size) {
        for (int i = 0; i < size; ++i) {
            values[i] = other.values[i];
        }
    }

    // move constructor, the same for move assigment operator
    ArrayWrapper(ArrayWrapper &&other) : values(other.values), size(other.size) {
        other.values = nullptr;
        other.size = 0;
    }


    ~ArrayWrapper() {
        delete[] values;
    }

};


#endif //CPP_MODERN_FEATURES_ARRAYWRAPPER_H
