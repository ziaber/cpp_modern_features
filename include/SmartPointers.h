//
// Created by Marcin Ziaber on 2019-07-09.
//

#ifndef CPP_MODERN_FEATURES_SMARTPOINTERS_H
#define CPP_MODERN_FEATURES_SMARTPOINTERS_H

#include <memory>

using namespace std;

class SmartPointers {
public:
    void uniquePtrExample();

    void sharedPtrExample();

    void uniquePtrAsArgumentExample(const unique_ptr<int> &arg);

    void uniquePtrAsArgumentMoveExample(unique_ptr<int> arg);

    void sharedPointerAsValueExample(const shared_ptr<int> arg);

};


#endif //CPP_MODERN_FEATURES_SMARTPOINTERS_H
