//
// Created by Marcin Ziaber on 2019-07-09.
//

#ifndef CPP_MODERN_FEATURES_TYPEINFERENCE_H
#define CPP_MODERN_FEATURES_TYPEINFERENCE_H


class TypeInference {
public:
    void typeInferenceExample();

    void autoWithRangeLoopExample();

    void decltypeExample();

    template<class T, class U>
    auto add(T t, U u) { return t + u; }

};


#endif //CPP_MODERN_FEATURES_TYPEINFERENCE_H
