//
// Created by Marcin Ziaber on 24/08/2019.
//

#ifndef CPP_MODERN_FEATURES_VARIADICTEMPLATE_H
#define CPP_MODERN_FEATURES_VARIADICTEMPLATE_H

#include <iostream>

using namespace std;

class VariadicTemplate {
public:
    void printAll() {}; //do nothing

    template<typename T, typename... Tail>
    void printAll(T head, Tail... tail) {
        print(head); // do something to head
        this->printAll(tail...); // try again with tail
    }

    template<typename T>
    void print(T toPrint) {
        cout << toPrint << endl;
    }

    void variadicTemplateExample();
};


#endif //CPP_MODERN_FEATURES_VARIADICTEMPLATE_H
