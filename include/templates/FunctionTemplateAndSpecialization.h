//
// Created by Marcin Ziaber on 24/08/2019.
//

#ifndef CPP_MODERN_FEATURES_FUNCTIONTEMPLATEANDSPECIALIZATION_H
#define CPP_MODERN_FEATURES_FUNCTIONTEMPLATEANDSPECIALIZATION_H


class FunctionTemplateAndSpecialization {
public:
    template<typename T>
    T max(T a, T b) {
        return a > b ? a : b;
    }

    template<>
    bool max<bool>(bool a, bool b) {
        return a || b;
    }


    void functionTemplateExample();
};


#endif //CPP_MODERN_FEATURES_FUNCTIONTEMPLATEANDSPECIALIZATION_H
