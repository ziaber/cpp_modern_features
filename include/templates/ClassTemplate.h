//
// Created by Marcin Ziaber on 24/08/2019.
//

#ifndef CPP_MODERN_FEATURES_CLASSTEMPLATE_H
#define CPP_MODERN_FEATURES_CLASSTEMPLATE_H

template<class T>
class ClassTemplate {
private:
    T t;
public:
    T max(T a, T b) {
        return a > b ? a : b;
    }

    void classTemplateExample();
};


#endif //CPP_MODERN_FEATURES_CLASSTEMPLATE_H
