//
// Created by Marcin Ziaber on 24/08/2019.
//

#ifndef CPP_MODERN_FEATURES_COMPILETIMEFACTORIAL_H
#define CPP_MODERN_FEATURES_COMPILETIMEFACTORIAL_H

template<unsigned int n>
struct factorial {
    enum {
        value = n * factorial<n - 1>::value
    };
};

template<>
struct factorial<0> {
    enum {
        value = 1
    };
};


#endif //CPP_MODERN_FEATURES_COMPILETIMEFACTORIAL_H
