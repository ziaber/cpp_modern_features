//
// Created by Marcin Ziaber on 24/08/2019.
//

#ifndef CPP_MODERN_FEATURES_TEMPLATEALIASES_H
#define CPP_MODERN_FEATURES_TEMPLATEALIASES_H


#include <unordered_map>

using namespace std;

class TemplateAliases {
public:
    template<class T>
    using StrMap = unordered_map<T, string>;

    void aliasesExample();

};


#endif //CPP_MODERN_FEATURES_TEMPLATEALIASES_H
