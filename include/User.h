//
// Created by Marcin Ziaber on 30/10/2019.
//

#ifndef CPP_MODERN_FEATURES_USER_H
#define CPP_MODERN_FEATURES_USER_H


class User {
private:
    int id;

public:
    User(int id);

    int getId() const;
};


#endif //CPP_MODERN_FEATURES_USER_H
